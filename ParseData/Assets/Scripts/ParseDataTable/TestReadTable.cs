﻿
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class TestReadTable : MonoBehaviour
{
    public Text tips;
    // Use this for initialization
    void Start()
    {
        Dictionary<string, string> infoDic = new Dictionary<string, string>();
		string filePath = Application.dataPath + "/Resources/File/2.txt";

		//正则解析
		ParseDataHelper.ParseDataTable(filePath,ref infoDic);

		//自定义解析
		//string msg = File.ReadAllText(filePath);
		//infoDic = ParseDataHelper.ParseDatatable(msg);

		foreach (var item in infoDic)
		{
			Debug.Log("key = "+item.Key + " value = " + item.Value);
		}
    }
}
